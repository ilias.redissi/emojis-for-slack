var OSName = "Unknown OS";
if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";

var emojiDefault = 'google';
if (OSName === 'MacOS') {
    emojiDefault = 'apple';
}

var noMouth = document.createElement('div');
noMouth.innerHTML = '<span class="emoji-outer emoji-sizer" style="margin: 4px; background-image: url(https://a.slack-edge.com/c00d19/img/emoji_2017_12_06/sheet_' + emojiDefault + '_64_indexed_256.png);background-position:60.78431372549019% 50.98039215686274%;background-size:5200% 5200%;" data-codepoints="1f636"></span>';
noMouth = noMouth.firstChild;

var localStorage = window.localStorage;

var emojistyles = {
    'apple': {
        'name': 'Apple',
        'url': 'https://a.slack-edge.com/c00d19/img/emoji_2017_12_06/sheet_apple_64_indexed_256.png'
    },
    'google': {
        'name': 'Google (Slack version)',
        'url': 'https://a.slack-edge.com/c00d19/img/emoji_2017_12_06/sheet_google_64_indexed_256.png'
    },
    'emojione': {
        'name': 'Emojione',
        'url': 'https://redissi.xyz/emojione'
    },
    'truegoogle': {
        'name': 'Google',
        'url': 'https://redissi.xyz/google'
    },
    'oldblobs': {
        'name': 'Google (old blobs version)',
        'url': 'https://redissi.xyz/oldblobs'
    }
}

function deleteMenu (menu) {
    document.getElementById('client-ui').removeChild(menu);
}

function changeStyle (style) {
    let emojiStyle = document.querySelector('#emoji-style');
    if (!emojiStyle) {
        emojiStyle = document.createElement('style');
        emojiStyle.id = "emoji-style";
        document.head.appendChild(emojiStyle);
    }



    let styleClasses = ".emoji-sizer[style*='sheet'], .emoji[style*='sheet']";


    if (!(style in emojistyles)) {
        style = 'emojione';
    }

    emojiStyle.innerText = styleClasses + " { background-image: url('" + emojistyles[style].url + "') !important; }";
}

function generateMenu (button) {
    let menu = Handlebars.compile(TS.templates['menu']())();
    let newElement = document.createElement('div');
    newElement.innerHTML = menu;
    newElement = newElement.firstChild;
    newElement.classList.add('narrow_menu');

    let positionInfo = button.getBoundingClientRect();
    newElement.style.top = (positionInfo.top + positionInfo.height) + 'px';
    newElement.style.left = positionInfo.left + 'px';
    newElement.style.marginTop = '8px';
    newElement.style.opacity = 1;

    newElement.querySelector('#menu_header').classList.add('hidden');

    let list = newElement.querySelector('#menu_items');

    for (let key in emojistyles) {
        let item = document.createElement('li');
        item.style.marginLeft = '8px';
        let label = document.createElement('label');
        label.classList.add('radio');
        let input = document.createElement('input');
        input.type = 'radio';
        input.id = key;
        input.name = 'emoji';
        input.value = key;
        input.checked = localStorage.getItem('emoji') ? localStorage.getItem('emoji') == key : false;
        input.onclick = function (event) {
            let value = event.target.value;
            localStorage.setItem('emoji', value);
            changeStyle(value);
            deleteMenu(newElement);
        }
        label.appendChild(input);
        label.appendChild(document.createTextNode(emojistyles[key].name));
        item.appendChild(label);
        list.appendChild(item);
    }

    newElement.querySelector('.popover_mask').onclick = function () {
        deleteMenu(newElement);
    }

    document.getElementById('client-ui').appendChild(newElement);
};


var checkExist = setInterval(function () {
    let infoElement = document.body.querySelector('.channel_title_info');
    if (infoElement) {
        let emojiElement = infoElement.querySelector("#emoji");
        if (!emojiElement) {
            let button = document.createElement('div');
            button.classList.add('channel_header_icon');
            button.id = 'emoji';
            button.onclick = function () {
                generateMenu(button);
            }
            button.appendChild(noMouth);

            infoElement.appendChild(button);
        }
    }
}, 1000);

if (!localStorage.getItem('emoji')) {
    localStorage.setItem('emoji', 'emojione');
}

//changeStyle(localStorage.getItem('emoji'));
changeStyle('emojione');