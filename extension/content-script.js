var s = document.createElement('script');
s.src = browser.extension.getURL('purify.min.js');
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);

s = document.createElement('script');
s.src = browser.extension.getURL('emojis-for-slack.js');
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);