# Emojis for Slack

This folder is a fork of [old-slack-emojis](https://github.com/IvyBits/old-slack-emojis)

## Installation

Clone this repository and run the script.

```shell
sudo bash emojis-for-slack.sh
```

## Uninstallation

To uninstall, run the script with `-u` as a flag.

## Updating Slack

Emojis for Slack injects some code into the Slack client, which may be overwritten when Slack updates. If you start seeing the new
emojis, rerunning the installation script should fix things.

## "Cannot find Slack installation"

If you've installed Slack in some exotic place, the script might not find the installation by itself or it might find the
wrong installation. In such cases, you need to specify the location of Slack's `app.asar.unpacked/src/static` folder as a parameter:

```shell
sudo bash emojis-for-slack.sh /My_Apps/Slack.app/Contents/Resources/app.asar.unpacked/src/static
```

## Credits

Emojis for Slack uses the same injection mechanism as [math-with-slack](https://github.com/fsavje/math-with-slack), without which
a lot more time would have gone into figuring out how to get the old spritesheet injected. Thanks!
