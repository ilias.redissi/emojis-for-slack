#!/usr/bin/env bash

## Functions

error() {
	echo "$(tput setaf 124)$(tput bold)✘ $1$(tput sgr0)"
	exit 1
}


## User input

for p in "$@"; do
	if [ "$p" = "-u" ]; then
		UNINSTALL="$p"
	else
		SLACK_DIR="$p"
	fi
done


## Platform settings

if [ "$(uname)" == "Darwin" ]; then
	# macOS
	COMMON_SLACK_LOCATIONS=(
		"/Applications/Slack.app/Contents/Resources/app.asar.unpacked/src/static"
	)
else
	# Linux
	COMMON_SLACK_LOCATIONS=(
		"/usr/lib/slack/resources/app.asar.unpacked/src/static"
		"/usr/local/lib/slack/resources/app.asar.unpacked/src/static"
		"/opt/slack/resources/app.asar.unpacked/src/static"
	)
fi


## Try to find slack if not provided by user

if [ -z "$SLACK_DIR" ]; then
	for loc in "${COMMON_SLACK_LOCATIONS[@]}"; do
		if [ -e "$loc" ]; then
			SLACK_DIR="$loc"
			break
		fi
	done
fi

## Files

SLACK_EMOJI_SCRIPT="$SLACK_DIR/emojis-for-slack.js"
SLACK_SSB_INTEROP="$SLACK_DIR/ssb-interop.js"


## Check so installation exists and is writable

if [ -z "$SLACK_DIR" ]; then
	error "Cannot find Slack installation."
elif [ ! -e "$SLACK_DIR" ]; then
	error "Cannot find Slack installation at: $SLACK_DIR"
elif [ ! -e "$SLACK_SSB_INTEROP" ]; then
	error "Cannot find Slack file: $SLACK_SSB_INTEROP"
elif [ ! -w "$SLACK_SSB_INTEROP" ]; then
	error "Cannot write to Slack file: $SLACK_SSB_INTEROP"
fi

echo "Using Slack installation at: $SLACK_DIR"


## Remove previous version

if [ -e "$SLACK_EMOJI_SCRIPT" ]; then
	rm $SLACK_EMOJI_SCRIPT
fi


## Restore previous injections

# Check whether file been injected. If not, assume it's more recent than backup
if grep -q "math-with-slack" $SLACK_SSB_INTEROP; then
	if [ -e "$SLACK_SSB_INTEROP.mwsbak" ]; then
    	mv -f $SLACK_SSB_INTEROP.mwsbak $SLACK_SSB_INTEROP
	else
    	error "Cannot restore from backup. Missing file: $SLACK_SSB_INTEROP.mwsbak"
	fi
elif [ -e "$SLACK_SSB_INTEROP.mwsbak" ]; then
	rm $SLACK_SSB_INTEROP.mwsbak
fi


## Are we uninstalling?

if [ -n "$UNINSTALL" ]; then
	echo "$(tput setaf 64)math-with-slack has been uninstalled. Please restart the Slack client.$(tput sgr0)"
	exit 0
fi


## Write main script

cp ../extension/emojis-for-slack.js $SLACK_EMOJI_SCRIPT


## Inject code loader

# Check so not already injected
if grep -q "math-with-slack" $SLACK_SSB_INTEROP; then
	error "File already injected: $SLACK_SSB_INTEROP"
fi

# Make backup
if [ ! -e "$SLACK_SSB_INTEROP.mwsbak" ]; then
	cp $SLACK_SSB_INTEROP $SLACK_SSB_INTEROP.mwsbak
else
	error "Backup already exists: $SLACK_SSB_INTEROP.mwsbak"
fi

# Inject loader code
ed -s $SLACK_SSB_INTEROP <<EOF > /dev/null
/init(resourcePath, mainModule, !isDevMode);
i
	// ** math-with-slack $MWS_VERSION ** https://github.com/fsavje/math-with-slack
	var mwsp = path.join(__dirname, 'emojis-for-slack.js').replace('app.asar', 'app.asar.unpacked');
	require('fs').readFile(mwsp, 'utf8', (e, r) => { if (e) { throw e; } else { eval(r); } });
.
w
q
EOF


## We're done

echo "$(tput setaf 64)math-with-slack has been installed. Please restart the Slack client.$(tput sgr0)"

